package org.inkubator.radinaldn.futurebasspad.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import org.inkubator.radinaldn.futurebasspad.Adapter.PresetAdapter;
import org.inkubator.radinaldn.futurebasspad.Fragment.PresetFragment;
import org.inkubator.radinaldn.futurebasspad.Model.Preset;
import org.inkubator.radinaldn.futurebasspad.R;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private ActionBar toolbar;

    private InterstitialAd mInterstitialAd;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
//                    mTextMessage.setText(R.string.title_activity_menu);
//                    toolbar.setTitle(R.string.title_activity_menu);
//                    return true;
break;
                case R.id.navigation_dashboard:
//                    mTextMessage.setText(R.string.title_dashboard);
                    Intent i = new Intent(MenuActivity.this, BiruActivity.class);
                    startActivity(i);
                    break;
                case R.id.navigation_notifications:
//                    mTextMessage.setText(R.string.title_notifications);
                    Intent i2 = new Intent(MenuActivity.this, SomethingJustLikeThisActivity.class);
                    startActivity(i2);
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        MobileAds.initialize(this, "ca-app-pub-7054861126359518~5006101670");

        mInterstitialAd  = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7054861126359518/1614485728");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        //toolbar = getSupportActionBar();

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

//        toolbar.setTitle("Preset");
        loadFragment(new PresetFragment());

    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
