package org.inkubator.radinaldn.futurebasspad.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.inkubator.radinaldn.futurebasspad.Activity.BiruActivity;
import org.inkubator.radinaldn.futurebasspad.Model.Preset;
import org.inkubator.radinaldn.futurebasspad.R;

import java.util.ArrayList;

/**
 * Created by radinaldn on 04/05/18.
 */

public class PresetAdapter extends RecyclerView.Adapter<PresetAdapter.PresetViewHolder> {

    private ArrayList<Preset> dataList;

    public PresetAdapter(ArrayList<Preset>  dataList){
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public PresetAdapter.PresetViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_preset, parent, false);
        return new PresetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PresetViewHolder holder, int position) {
        holder.tvjudul.setText(dataList.get(position).getJudul());
        holder.tvisi.setText(dataList.get(position).getIsi());
        Picasso.get().load(dataList.get(position).getGambar()).into(holder.ivgambar);

    }

    @Override
    public int getItemCount() {
        return (dataList != null) ? dataList.size() : 0;
    }

    public class PresetViewHolder extends RecyclerView.ViewHolder{
        private TextView tvjudul, tvisi;
        private ImageView ivgambar;

        public PresetViewHolder(View view){
            super(view);
            tvjudul = (TextView) itemView.findViewById(R.id.tvjudul);
            tvisi = (TextView) itemView.findViewById(R.id.tvisi);
            ivgambar = (ImageView) itemView.findViewById(R.id.ivgambar);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    switch (tvjudul.getText().toString()){
                        case "CVX - Biru":
                            Intent i = new Intent(v.getContext(), BiruActivity.class);
                            v.getContext().startActivity(i);
                            break;

                            default:
                                Toast.makeText(v.getContext(), "Mohon maaf "+tvjudul.getText().toString()+" belum tersedia.", Toast.LENGTH_SHORT).show();


                    }

                }
            });
        }


    }
}
