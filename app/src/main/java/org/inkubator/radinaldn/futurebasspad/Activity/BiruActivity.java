package org.inkubator.radinaldn.futurebasspad.Activity;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import org.inkubator.radinaldn.futurebasspad.R;

public class BiruActivity extends AppCompatActivity {

    Button btnchord1, btnchord2, btnchord3, btnchord4, btnchord5, btnperc1,
            btnperc2, btnkick, btnsnare, btnclap, btnleadbass1, btnleadbass2;
    private SoundPool sp, sp2;
    private int chord1;
    private int chord2;
    private int chord3;
    private int chord4;
    private int chord5;
    private int perc1;
    private int perc2;
    private int kick;
    private int snare;
    private int clap;
    private int lead_bass1;
    private int lead_bass2;

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ca-app-pub-7054861126359518~5006101670
        MobileAds.initialize(this, "ca-app-pub-7054861126359518~5006101670");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        btnchord1 = findViewById(R.id.orange1);
        btnchord2= findViewById(R.id.orange2);
        btnchord3 = findViewById(R.id.orange3);
        btnchord4 = findViewById(R.id.btsynth1);
        btnchord5 = findViewById(R.id.btsynth2);
        btnperc1 = findViewById(R.id.red1);
        btnperc2 = findViewById(R.id.red2);
        btnkick = findViewById(R.id.btkick);
        btnsnare = findViewById(R.id.btsnare);
        btnclap = findViewById(R.id.btclap);
        btnleadbass1 = findViewById(R.id.btsynth3);
        btnleadbass2 = findViewById(R.id.red3);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sp = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();

            sp2 = new SoundPool.Builder()
                    .setMaxStreams(12)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            sp = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
            sp2 = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        }

        chord1 = sp.load(this, R.raw.chord1, 1);
        chord2 = sp.load(this, R.raw.chord2, 1);
        chord3 = sp.load(this, R.raw.chord3, 1);
        chord4 = sp.load(this, R.raw.chord4, 1);
        chord5 = sp.load(this, R.raw.chord5, 1);
        perc1 = sp.load(this, R.raw.perc1, 1);
        perc2 = sp.load(this, R.raw.perc2, 1);
        kick = sp.load(this, R.raw.kick1, 1);
        snare = sp.load(this, R.raw.snare, 1);
        clap = sp.load(this, R.raw.clap, 1);
        lead_bass1 = sp2.load(this, R.raw.lead_bass1, 1);
        lead_bass2 = sp2.load(this, R.raw.lead_bass2, 1);

        btnchord1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord3, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord4, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord5, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnperc1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(perc1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnperc2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(perc2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnkick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(kick, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnsnare.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(snare, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnclap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(clap, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnleadbass1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead_bass1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnleadbass2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead_bass2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.release();
        sp2.release();
        sp = null;
        sp2 = null;
    }

}
