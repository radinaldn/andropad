package org.inkubator.radinaldn.futurebasspad.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.inkubator.radinaldn.futurebasspad.Activity.MenuActivity;
import org.inkubator.radinaldn.futurebasspad.Adapter.PresetAdapter;
import org.inkubator.radinaldn.futurebasspad.Model.Preset;
import org.inkubator.radinaldn.futurebasspad.R;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * Created by radinaldn on 04/05/18.
 */

public class PresetFragment extends Fragment {

    private RecyclerView recyclerView;
    private PresetAdapter adapter;
    private ArrayList<Preset> presetArrayList;

    public PresetFragment(){

    }

    public static PresetFragment newInstance(String param1, String param2) {
        PresetFragment fragment = new PresetFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        View view=  inflater.inflate(R.layout.fragment_preset, container, false);
        Log.i(TAG, "onCreateView: inflate fragment_preset berhasil");

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        adapter = new PresetAdapter(presetArrayList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        addData();
        Log.i(TAG, "onCreateView: add data berhasil");

        return view;
    }

    private void addData() {
        presetArrayList = new ArrayList<>();
        presetArrayList.add(new Preset("CVX - Biru", "Single CVX berjudul biru", R.drawable.biru_cover));
        presetArrayList.add(new Preset("Rendy Pandugo - Silver Rain (CVX Remix)", "Remix contest from uprising", R.drawable.silverrain_cover));
    }
}
