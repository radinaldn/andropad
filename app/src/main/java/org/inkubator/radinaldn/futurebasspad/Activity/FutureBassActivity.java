package org.inkubator.radinaldn.futurebasspad.Activity;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import org.inkubator.radinaldn.futurebasspad.R;

public class FutureBassActivity extends AppCompatActivity {

    Button btnsynth1, btnsynth2, btnsynth3, btnkick, btnsnare;
    private SoundPool sp;
    private int synth1;
    private int synth2;
    private int synth3;
    private int kick;
    private int snare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnsynth1 = findViewById(R.id.btsynth1);
        btnsynth2 = findViewById(R.id.btsynth2);
        btnsynth3 = findViewById(R.id.btsynth3);
        btnkick = findViewById(R.id.btkick);
        btnsnare = findViewById(R.id.btsnare);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sp = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            sp = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
        }

        synth1 = sp.load(this, R.raw.synth1, 1);
        synth2 = sp.load(this, R.raw.synth2, 1);
        synth3 = sp.load(this, R.raw.synth3, 1);
        kick = sp.load(this, R.raw.kick1, 1);
        snare = sp.load(this, R.raw.snare, 1);

        btnsynth1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(synth1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnsynth2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(synth2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnsynth3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(synth3, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnkick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(kick, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnsnare.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(snare, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.release();
        sp = null;
    }

}
