package org.inkubator.radinaldn.futurebasspad.Model;

/**
 * Created by radinaldn on 04/05/18.
 */

public class Preset {
    private String judul;
    private String isi;
    private int gambar;

    public Preset(String judul, String isi, int gambar) {
        this.judul = judul;
        this.isi = isi;
        this.gambar = gambar;
    }

    public String getJudul() {

        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public int getGambar() {
        return gambar;
    }

    public void setGambar(int gambar) {
        this.gambar = gambar;
    }
}
