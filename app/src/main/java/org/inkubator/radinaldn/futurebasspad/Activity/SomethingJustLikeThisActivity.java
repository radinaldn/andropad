package org.inkubator.radinaldn.futurebasspad.Activity;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import org.inkubator.radinaldn.futurebasspad.R;

public class SomethingJustLikeThisActivity extends AppCompatActivity {

    Button btnchord1, btnchord2, btnchord3, btnlead1, btnlead2, btnlead3, btnlead4, btncrash, btnkick, btnclap;
    private SoundPool sp, sp2;
    private int chord1;
    private int chord2;
    private int chord3;
    private int lead1;
    private int lead2;
    private int lead3;
    private int lead4;
    private int crash;
    private int kick;
    private int clap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnchord1 = findViewById(R.id.orange1);
        btnchord2= findViewById(R.id.orange2);
        btnchord3 = findViewById(R.id.orange3);
        btnlead1 = findViewById(R.id.btsynth1);
        btnlead2 = findViewById(R.id.btsynth2);
        btnlead3 = findViewById(R.id.btsynth3);
        btnlead4 = findViewById(R.id.red1);
        btnkick = findViewById(R.id.btkick);
        btncrash = findViewById(R.id.btsnare);
        btnclap = findViewById(R.id.btclap);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sp = new SoundPool.Builder()
                    .setMaxStreams(6)
                    .setAudioAttributes(audioAttributes)
                    .build();

            sp2 = new SoundPool.Builder()
                    .setMaxStreams(12)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            sp = new SoundPool(6, AudioManager.STREAM_MUSIC, 0);
            sp2 = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
        }

        chord1 = sp.load(this, R.raw.sjlt_chord1, 1);
        chord2 = sp.load(this, R.raw.sjlt_chord2, 1);
        chord3 = sp.load(this, R.raw.sjlt_chord3, 1);
        lead1 = sp2.load(this, R.raw.sjlt_lead1, 1);
        lead2 = sp2.load(this, R.raw.sjlt_lead2, 1);
        lead3 = sp2.load(this, R.raw.sjlt_lead3, 1);
        lead4 = sp2.load(this, R.raw.sjlt_lead4, 1);
        kick = sp.load(this, R.raw.sjlt_kick, 1);
        crash = sp.load(this, R.raw.sjlt_crash, 1);
        clap = sp.load(this, R.raw.sjlt_clap, 1);

        btnchord1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnchord3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(chord3, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnlead1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead1, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnlead2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead2, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnlead3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead3, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnlead4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp2.play(lead4, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnkick.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(kick, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btncrash.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(crash, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });

        btnclap.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // PRESSED
                        sp.play(clap, 1, 1, 0, 0, 1);
                        return true; // if you want to handle the touch event
                    case MotionEvent.ACTION_UP:
                        // RELEASED
                        return true; // if you want to handle the touch event
                }
                return false;
            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        sp.release();
        sp2.release();
        sp = null;
        sp2 = null;
    }

}
