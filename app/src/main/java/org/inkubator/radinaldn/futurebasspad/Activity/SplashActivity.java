package org.inkubator.radinaldn.futurebasspad.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.inkubator.radinaldn.futurebasspad.R;


public class SplashActivity extends AppCompatActivity {
    TextView tvSplash;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //menghilangkan ActionBar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        progressBar=  findViewById(R.id.progresBar1);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.blueendcolor), PorterDuff.Mode.MULTIPLY);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), BiruActivity.class));
                finish();
            }
        }, 2000); //2000 L = 2 detik
    }
}